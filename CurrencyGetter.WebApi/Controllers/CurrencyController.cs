﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyGetter.WebApi.Controllers
{
    using CurrencyGetter.BAL.Core.Interfaces;

    [ApiController]
    [Route("api/[controller]")]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyManager currencyManager;

        public CurrencyController(ICurrencyManager currencyManager) => this.currencyManager = currencyManager;


        [HttpGet("codes")]
        public Task<IDataResult<IEnumerable<string>>> GetCurrencyCodesAsync() => currencyManager.GetCurrencyCodesAsync();

        [HttpGet("{code}/rate")]
        public Task<IDataResult<float>> GetCurrencyRateByCodeAsync(string code) => currencyManager.GetCurrencyRateByCodeAsync(code);
    }
}
