﻿using System;
using Microsoft.Extensions.Configuration;

namespace CurrencyGetter.Helpers.Extensions
{
    using Microsoft.Extensions.DependencyInjection;

    public static class ServiceCollectionExtensions
    {
        public static IHttpClientBuilder AddHttpClient<T, TM>(
            this IServiceCollection services,
            IConfiguration configuration,
            string baseAddressConfig)
            where T : class where TM : class, T
        {
            var uri = GetUriFromConfig(baseAddressConfig, configuration);
            return services.AddHttpClient<T, TM>(client => { client.BaseAddress = uri; });
        }

        private static Uri GetUriFromConfig(string baseAddressConfig, IConfiguration configuration) => new Uri(configuration.GetSection("UriConfiguration").GetSection(baseAddressConfig).Value);
    }
}
