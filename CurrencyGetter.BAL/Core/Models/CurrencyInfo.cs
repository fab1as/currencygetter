﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyGetter.BAL.Core.Models
{
    public class CurrencyInfo
    {
        public string Id { get; set; }

        public string NumCode { get; set; }

        public string CharCode { get; set; }

        public int Nominal { get; set; }

        public string Name { get; set; }

        public float Value { get; set; }

        public float Previous { get; set; }
    }
}
