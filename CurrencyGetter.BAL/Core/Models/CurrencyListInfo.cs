﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyGetter.BAL.Core.Models
{
    public class CurrencyListInfo
    {
        public DateTime Date { get; set; }

        public DateTime PreviousDate  { get; set; }

        public DateTime Timestamp { get; set; }

        public string PreviousURL { get; set; }

        public Dictionary<string, CurrencyInfo> Valute { get; set; }
    }
}
