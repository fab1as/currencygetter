﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyGetter.BAL.Core.Interfaces
{
    public interface IDataResult<T>
    {
        bool Success { get; }

        string Message { get; }

        T Data { get; }
    }
}
