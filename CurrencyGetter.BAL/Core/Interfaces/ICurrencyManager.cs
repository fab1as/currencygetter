﻿namespace CurrencyGetter.BAL.Core.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ICurrencyManager
    {
        Task<IDataResult<IEnumerable<string>>> GetCurrencyCodesAsync();

        Task<IDataResult<float>> GetCurrencyRateByCodeAsync(string code);
    }
}
