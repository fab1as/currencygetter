﻿namespace CurrencyGetter.BAL
{
    using CurrencyGetter.BAL.Core.Interfaces;

    public class DataResult<T> : IDataResult<T>
    {
        public DataResult(T data)
        {
            Success = true;
            Data = data;
        }

        public DataResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }

        public bool Success { get; }

        public string Message { get; }

        public T Data { get; }
    }
}
