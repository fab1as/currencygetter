﻿namespace CurrencyGetter.BAL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    using CurrencyGetter.BAL.Core.Interfaces;
    using CurrencyGetter.BAL.Core.Models;

    using Microsoft.Extensions.Logging;

    public class CurrencyManager : ICurrencyManager
    {
        private readonly ILogger<CurrencyManager> logger;
        private readonly HttpClient httpClient;
        private readonly string fullAddress;

        public CurrencyManager(HttpClient httpClient, ILogger<CurrencyManager> logger)
        {
            this.logger = logger;
            this.httpClient = httpClient;
            fullAddress = $"{httpClient.BaseAddress}";
        }

        public async Task<IDataResult<IEnumerable<string>>> GetCurrencyCodesAsync()
        {
            var currencyListInfoResult = await GetCurrencyInfoAsync().ConfigureAwait(false);
            if (!currencyListInfoResult.Success)
                return new DataResult<IEnumerable<string>>(false, currencyListInfoResult.Message);
            return new DataResult<IEnumerable<string>>(currencyListInfoResult.Data.Valute.Keys);
        }

        public async Task<IDataResult<float>> GetCurrencyRateByCodeAsync(string code)
        {
            var currencyListInfoResult = await GetCurrencyInfoAsync().ConfigureAwait(false);
            if (!currencyListInfoResult.Success)
                return new DataResult<float>(false, currencyListInfoResult.Message);
            if (!currencyListInfoResult.Data.Valute.TryGetValue(code, out var currencyInfo))
                return new DataResult<float>(false, $"Валюта с кодом {code} отсутствует");
            return new DataResult<float>(currencyInfo.Value);
        }

        private async Task<IDataResult<CurrencyListInfo>> GetCurrencyInfoAsync()
        {
            try
            {
                var data = await httpClient.GetStringAsync($"{fullAddress}/daily_json.js").ConfigureAwait(false);
                return new DataResult<CurrencyListInfo>(JsonConvert.DeserializeObject<CurrencyListInfo>(data));
            }
            catch (Exception exception)
            {
                var errorString = "Произошла ошибка при попытке получить данные";
                logger.LogError(exception, errorString);
                return new DataResult<CurrencyListInfo>(false, errorString);
            }
        }
    }
}
